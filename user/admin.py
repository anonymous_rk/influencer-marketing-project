from django.contrib import admin
from .models import CustomUser as User, Role


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ("username", 'first_name', 'last_name', 'email', 'phone_number', 'role')


@admin.register(Role)
class RoleAdmin(admin.ModelAdmin):
    list_display = ('name_uz', )


# admin.site.register(User, UserAdmin)
# admin.site.register(Role, RoleAdmin)
