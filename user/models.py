from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.db import models
from django.utils.translation import gettext_lazy as _

from .managers import CustomUserManager
from stats.models import Platform
from main.models import Category


class Role(models.Model):
    name_uz = models.CharField(max_length=50)
    name_ru = models.CharField(max_length=50)
    name_en = models.CharField(max_length=50)

    class Meta:
        verbose_name = "Role"
        verbose_name_plural = "Roles"


class CustomUser(AbstractBaseUser, PermissionsMixin):

    CATEGORY_CHOICES = (
        ('Fashion&Beauty', 'Fashion'),
        ('Education', 'Education'),
        ('Food', 'Food'),
        ('Sports', 'Sports'),
        ('Travel', 'Travel'),
        ('Health&Fitness', 'Fitness'),
        ('Finance', 'Finance'),
        ('Politics', 'Politics'),
        ('Business', 'Business'),
        ('Movie', 'Movie'),
        ('Car', 'Car'),
        ('News', 'News'),
        ('Gaming', 'Gaming'),
        ('Psychology', 'Psychology'),
        ('Personal', 'Personal'),
        ('Design&Art', 'Design&Art'),
        ('Books', 'Books'),
        ('DIY', 'DIY'),
        ('Construction', 'Construction'),
        ('Electronics', 'Electronics'),
        ('IT&Programming', 'IT&Programming'),
    )

    username = models.CharField(
        _('username'),
        unique=True,
        max_length=150,
        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[UnicodeUsernameValidator],
        error_messages={
            'unique': _("User is already registered by this username."),
        }

    )

    first_name = models.CharField(blank=True, max_length=150, verbose_name='first name'),
    last_name = models.CharField(blank=True, max_length=150, verbose_name='last name'),
    email = models.EmailField(_('email address'), unique=True, error_messages={'unique': _("User is already registered by this email.")})
    phone_number = models.CharField(_('phone number'), max_length=20)
    user_platforms = models.ManyToManyField(Platform, help_text=_("User's main business platform as a type of social site."), default='Not chosen')
    company = models.CharField(max_length=100, help_text=_("Your company name."))
    category = models.ForeignKey(Category, on_delete=models.CASCADE, help_text=_("Your main content on social sites."), default='Not chosen')
    how_find_us = models.CharField(max_length=255, help_text=_("How did you know about us?"))

    # relations

    role = models.ForeignKey(Role, on_delete=models.CASCADE)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return self.email
