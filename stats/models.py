from django.db import models


class Platform(models.Model):
    name_uz = models.CharField(max_length=50)
    name_ru = models.CharField(max_length=50)
    name_en = models.CharField(max_length=50)

    class Meta:
        verbose_name = "Main platform"
        verbose_name_plural = "Main platforms"


class Statistics(models.Model):
    rate = models.IntegerField()
    followers = models.BigIntegerField()

    class Meta:
        verbose_name = "Statistic"
        verbose_name_plural = "Statistics"
